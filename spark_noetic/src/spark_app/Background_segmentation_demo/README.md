## gaussian_mixture_based_segmentation
this function is powered by opencv. Also, you should start it with Opencv3. Attention: OpenCV3 must be configured or it will not be executed successfully.

## Get Started
```Python3
cd ~/catkin_ws/src
git clone https://github.com/ZhuChaozheng/gaussian_mixture_based_segmentation
cd gaussian_mixture_based_segmentation/script
python3 bg_sub.py
```
