# 夺宝奇兵比赛

## 介绍
夺宝奇兵比赛

## 遇到的一些问题
### ROS下运行出错
如果运行时出现了类似以下图片的保错，

![error](pictures/error1.png)

需要给`~/spark_noetic/src/3rd_app/move2grasp/scripts`下的所有文件添加执行权限，在`~/spark_noetic/src/3rd_app/move2grasp/scripts`打开命令行输入：
```yaml
sudo chmod 777 -R ./
```
或
```yaml
sudo chmod +x -R ./
```
