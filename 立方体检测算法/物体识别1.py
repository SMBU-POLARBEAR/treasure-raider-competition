import cv2
import pyrealsense2 as rs
import numpy as np


def segment_blue_cubes(image):
    hsv_image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

    lower_blue = np.array([95, 90, 90])
    upper_blue = np.array([130, 255, 255])

    blue_mask = cv2.inRange(hsv_image, lower_blue, upper_blue)

    kernel = np.ones((15, 15), np.uint8)
    blue_mask = cv2.morphologyEx(blue_mask, cv2.MORPH_OPEN, kernel)

    contours, _ = cv2.findContours(blue_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    blue_cubes = []

    result = image.copy()
    for contour in contours:
        x, y, w, h = cv2.boundingRect(contour)
        blue_cubes.append((x, y, x+w, y+h))
        cv2.rectangle(result, (x, y), (x+w, y+h), (0, 255, 0), 2)

    cv2.imshow('result',result)








# 创建深度相机对象
pipeline = rs.pipeline()

# 配置深度相机
config = rs.config()
config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)

# 启动深度相机
pipeline.start(config)

while True:
    # 等待深度图像和彩色图像帧
    frames = pipeline.wait_for_frames()
    depth_frame = frames.get_depth_frame()
    color_frame = frames.get_color_frame()

    if not depth_frame or not color_frame:
        print("无法获取深度或彩色图像帧")
        break

    # 将深度图像转换为可视化图像
    depth_visual = cv2.normalize(np.array(depth_frame.get_data()), None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8UC1)

    # 获取彩色图像数据
    color_image = np.array(color_frame.get_data())
##################################################
    segment_blue_cubes(color_image)


##################################################
    # 显示深度图像和彩色图像
    # cv2.imshow("depth", depth_visual)
    # cv2.imshow("RGB", color_image)

    # 按下Esc键退出循环
    if cv2.waitKey(1) == 27:
        break

# 停止深度相机并释放资源
pipeline.stop()
cv2.destroyAllWindows()

