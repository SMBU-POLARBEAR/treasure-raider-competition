import cv2
import pyrealsense2 as rs
import numpy as np

def image_callback(cv_image1):
    # 进行图像处理和物体检测的逻辑...

    # change rgb to hsv
    cv_image2 = cv2.cvtColor(cv_image1, cv2.COLOR_BGR2HSV)
    
    # 蓝色物体颜色检测范围
    # LowerBlue = np.array([95, 150, 150])
    # UpperBlue = np.array([105, 255, 255])
    LowerBlue = np.array([95, 90, 90])
    UpperBlue = np.array([130, 255, 255])

    #图像处理
    mask = cv2.inRange(cv_image2, LowerBlue, UpperBlue)
    cv_image3 = cv2.bitwise_and(cv_image2, cv_image2, mask=mask)
    cv_image4 = cv_image3[:, :, 0]
    # cv2.imshow("cv_image2", cv_image2)
    # cv2.imshow("cv_image3", cv_image3)
    # cv2.imshow("cv_image4", cv_image4)
    
    

    blurred = cv2.blur(cv_image4, (3, 3))
    (_, thresh) = cv2.threshold(blurred, 90, 255, cv2.THRESH_BINARY)
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (25, 25))
    cv_image5 = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel)
    cv_image5 = cv2.erode(cv_image5, None, iterations=4)
    cv_image5 = cv2.dilate(cv_image5, None, iterations=4)
    # _, thresh = cv2.threshold(cv_image4, 90, 255, cv2.THRESH_BINARY)
    # cv_image5 = thresh.copy()
    # kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (25, 25))
    # cv_image5 = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel)
    # cv_image5 = cv2.erode(cv_image5, None, iterations=4)
    # cv_image5 = cv2.dilate(cv_image5, None, iterations=4)
        # detect contour
    # cv2.imshow("win1", cv_image1)
    cv2.imshow("cv_image5", cv_image5)
        # cv2.waitKey(1)

    # mask = cv_image5.copy()
    mask = cv2.dilate(cv_image5, np.ones((5,5), np.float32), iterations=1)
    # 进行边缘检测
    edges = cv2.Canny(cv_image1, threshold1=20, threshold2=300)
    cv2.imshow("edges", edges)

    cv_image6 = cv2.bitwise_and(cv_image2, cv_image2, mask=mask)
    cv2.imshow("cv_image6", cv_image6)
    cv_image7 = cv2.bitwise_and(edges, edges, mask=mask)
    cv2.imshow("cv_image7", cv_image7)



    contours, hier = cv2.findContours(cv_image5, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    # 存储检测到的物体的中心位置
    object_positions = []
    # 计算物体数量并绘制轮廓和中心位置
    object_count = len(contours)
    for contour in contours:
        # 计算轮廓的中心位置
        M = cv2.moments(contour)
        cx = int(M['m10'] / M['m00'])
        cy = int(M['m01'] / M['m00'])
        
        # 存储物体的中心位置
        object_positions.append((cx, cy))
            
        # 绘制轮廓和中心位置
        cv2.drawContours(cv_image1, [contour], -1, (0, 255, 0), 2)
        cv2.circle(cv_image1, (cx, cy), 4, (0, 0, 255), -1)

    
    # 显示处理结果
    cv2.imshow("Image", cv_image1)
    # cv2.waitKey(1)
        
        # 输出物体数量和中心位置
    print("Detected %d objects", object_count)
    print("Object positions: %s", object_positions)








    # kernel = np.ones((15,15), np.float32)
    # result1 = cv2.dilate(cv_image4,kernel)  #膨胀运算
    # mask = result1.copy()
    # # cv2.imshow("mask", mask)
    # # cv2.imshow("cv_image3", cv_image3)
    # cv2.imshow("cv_image4", cv_image4)
    # #减噪以及平滑
    # # blurred = cv2.blur(cv_image4, (3, 3))
    # # 
    # # (_, thresh) = cv2.threshold(blurred, 90, 255, cv2.THRESH_BINARY)
    # # kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (25, 25))
    # # mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)
    # # mask = cv2.erode(mask, None, iterations=4)
    # # mask = cv2.dilate(mask, None, iterations=4)

    # # contours, hier = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    
    
    # # 进行边缘检测
    # edges = cv2.Canny(cv_image1, threshold1=60, threshold2=200)
    # cv_image5 = cv2.bitwise_and(edges, edges, mask=mask)
    # cv2.imshow("cv_image5", cv_image5)

    # # 查找轮廓
    # contours, hierarchy = cv2.findContours(cv_image5, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # # 在原始图像上绘制边框
    # image_with_boxes = cv_image1.copy()

    # for contour in contours:
    #     area = cv2.contourArea(contour)
    #     if area < 60:
    #         continue
    #     x, y, w, h = cv2.boundingRect(contour)
    #     cv2.rectangle(image_with_boxes, (x, y), (x + w, y + h), (0, 255, 0), 2)
    # # 显示原始图像和边缘图像
    # # cv2.imshow("原始图像", image)
    # cv2.imshow("edge", edges)

    # cv2.imshow("rectangle", image_with_boxes)







# 创建深度相机对象
pipeline = rs.pipeline()

# 配置深度相机
config = rs.config()
config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)

# 启动深度相机
pipeline.start(config)

while True:
    # 等待深度图像和彩色图像帧
    frames = pipeline.wait_for_frames()
    depth_frame = frames.get_depth_frame()
    color_frame = frames.get_color_frame()

    if not depth_frame or not color_frame:
        print("无法获取深度或彩色图像帧")
        break

    # 将深度图像转换为可视化图像
    depth_visual = cv2.normalize(np.array(depth_frame.get_data()), None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8UC1)

    # 获取彩色图像数据
    color_image = np.array(color_frame.get_data())
##################################################
    image_callback(color_image)


##################################################
    # 显示深度图像和彩色图像
    # cv2.imshow("depth", depth_visual)
    # cv2.imshow("RGB", color_image)

    # 按下Esc键退出循环
    if cv2.waitKey(1) == 27:
        break

# 停止深度相机并释放资源
pipeline.stop()
cv2.destroyAllWindows()

