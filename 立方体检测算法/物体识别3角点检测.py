import cv2
import pyrealsense2 as rs
import numpy as np

def sharpen_image(image):
    # 创建拉普拉斯滤波器
    kernel = np.array([[0, -1, 0],
                       [-1, 5, -1],
                       [0, -1, 0]])
    
    # 对图像应用滤波器
    sharpened_image = cv2.filter2D(image, -1, kernel)
    return sharpened_image


def segment_blue_cubes(image):
    image_1 = sharpen_image(image)
    # cv2.imshow('sharpen_image',image_1)
    hsv_image = cv2.cvtColor(image_1, cv2.COLOR_BGR2HSV)

    lower_blue = np.array([95, 90, 90])
    upper_blue = np.array([130, 255, 255])

    blue_mask = cv2.inRange(hsv_image, lower_blue, upper_blue)
    # cv2.imshow('blue_mask',blue_mask)

    kernel = np.ones((15, 15), np.uint8)
    blue_mask = cv2.morphologyEx(blue_mask, cv2.MORPH_OPEN, kernel)

    # 进行边缘检测
    edges = cv2.Canny(image, 10, 150)
    mask = cv2.dilate(blue_mask, np.ones((3,3), np.float32), iterations=2)
    cv2.imshow('edges_mask',cv2.bitwise_and(image_1, image_1, mask=mask))
    edges = cv2.bitwise_and(edges, edges, mask=mask)
    cv2.imshow('edges',edges)

    # 进行Shi-Tomasi角点检测
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    corners = cv2.goodFeaturesToTrack(edges, maxCorners=100, qualityLevel=0.3, minDistance=7)

    # 将检测到的角点绘制在图像上
    # result = image.copy()
    # for corner in corners:
    #     x, y = corner.ravel()
    #     cv2.circle(result, (int(x), int(y)), 3, (0, 255, 0), -1)

    # cv2.imshow('result',result)








# 创建深度相机对象
pipeline = rs.pipeline()

# 配置深度相机
config = rs.config()
config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)

# 启动深度相机
pipeline.start(config)

while True:
    # 等待深度图像和彩色图像帧
    frames = pipeline.wait_for_frames()
    depth_frame = frames.get_depth_frame()
    color_frame = frames.get_color_frame()

    if not depth_frame or not color_frame:
        print("无法获取深度或彩色图像帧")
        break

    # 将深度图像转换为可视化图像
    depth_visual = cv2.normalize(np.array(depth_frame.get_data()), None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8UC1)

    # 获取彩色图像数据
    color_image = np.array(color_frame.get_data())
##################################################
    segment_blue_cubes(color_image)


##################################################
    # 显示深度图像和彩色图像
    # cv2.imshow("depth", depth_visual)
    # cv2.imshow("RGB", color_image)

    # 按下Esc键退出循环
    if cv2.waitKey(1) == 27:
        break

# 停止深度相机并释放资源
pipeline.stop()
cv2.destroyAllWindows()

