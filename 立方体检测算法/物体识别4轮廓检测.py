import cv2
import pyrealsense2 as rs
import numpy as np

def segment_blue_cubes(image):
    result = image.copy()
    hsv_image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

    lower_blue = np.array([95, 90, 90])
    upper_blue = np.array([130, 255, 255])

    blue_mask = cv2.inRange(hsv_image, lower_blue, upper_blue)

    kernel = np.ones((15, 15), np.uint8)
    blue_mask = cv2.morphologyEx(blue_mask, cv2.MORPH_OPEN, kernel)

    # 进行边缘检测
    edges = cv2.Canny(image, 40, 150)
    mask=cv2.dilate(blue_mask, np.ones((5,5), np.float32), iterations=2)
    edges_blue = cv2.bitwise_and(edges, edges, mask=mask)
    # 膨胀
    dilated = cv2.dilate(edges_blue, np.ones((7,7), np.float32), iterations=3)

    contours, hierarchy = cv2.findContours(dilated, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    
    # 绘制内轮廓
    center_list = []
    for i in range(len(contours)):
        if hierarchy[0, i, 3] != -1:
            contour = contours[i]
            # 计算最小矩形
            rect = cv2.minAreaRect(contour)
            box = cv2.boxPoints(rect)
            box = np.intp(box)
            width = rect[1][0];height = rect[1][1]
            aspect_ratio = width / height# 计算长宽比

            if 0.6 < aspect_ratio < 1.4 and width * height > 2000:
                # 计算中心点
                moments = cv2.moments(box)
                center_x = int(moments['m10'] / moments['m00'])
                center_y = int(moments['m01'] / moments['m00'])
                center_list.append((center_x,center_y))
                cv2.drawContours(result, [box], 0, (0, 255, 0), 2)# 绘制最小矩形
                cv2.circle(result, (center_x, center_y), 5, (0, 0, 255), -1)# 绘制中心点

    # cv2.imshow('dilated',dilated)
    # cv2.imshow('result',result)
    return (
        center_list,
        dilated,
        result
    )








# 创建深度相机对象
pipeline = rs.pipeline()

# 配置深度相机
config = rs.config()
config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)

# 启动深度相机
pipeline.start(config)

while True:
    # 等待深度图像和彩色图像帧
    frames = pipeline.wait_for_frames()
    depth_frame = frames.get_depth_frame()
    color_frame = frames.get_color_frame()

    if not depth_frame or not color_frame:
        print("无法获取深度或彩色图像帧")
        break

    # 将深度图像转换为可视化图像
    depth_visual = cv2.normalize(np.array(depth_frame.get_data()), None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8UC1)

    # 获取彩色图像数据
    color_image = np.array(color_frame.get_data())
##################################################
    center_list,dilated,result = segment_blue_cubes(color_image)
    print(center_list)
    cv2.imshow('dilated',dilated)
    cv2.imshow('result',result)

##################################################
    # 显示深度图像和彩色图像
    # cv2.imshow("depth", depth_visual)
    # cv2.imshow("RGB", color_image)

    # 按下Esc键退出循环
    if cv2.waitKey(1) == 27:
        break

# 停止深度相机并释放资源
pipeline.stop()
cv2.destroyAllWindows()

