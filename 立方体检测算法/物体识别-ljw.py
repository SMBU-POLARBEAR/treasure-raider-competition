import cv2
import pyrealsense2 as rs
import numpy as np

def segment_blue_cubes(image):
    result = image.copy()
    hsv_image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

    lower_blue = np.array([95, 90, 90])
    upper_blue = np.array([130, 255, 255])

    blue_mask = cv2.inRange(hsv_image, lower_blue, upper_blue)

    kernel = np.ones((15, 15), np.uint8)
    blue_mask = cv2.morphologyEx(blue_mask, cv2.MORPH_OPEN, kernel)

    # 进行边缘检测
    edges = cv2.Canny(image, 40, 150)
    mask=cv2.dilate(blue_mask, np.ones((5,5), np.float32), iterations=2)
    edges_blue = cv2.bitwise_and(edges, edges, mask=mask)
    # 膨胀
    dilated = cv2.dilate(edges_blue, np.ones((7,7), np.float32), iterations=3)

    contours, hierarchy = cv2.findContours(dilated, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    
    # 绘制内轮廓
    center_list = []
    for i in range(len(contours)):
        if hierarchy[0, i, 3] != -1:
            contour = contours[i]
            # 计算最小矩形
            rect = cv2.minAreaRect(contour)
            box = cv2.boxPoints(rect)
            box = np.intp(box)
            width = rect[1][0];height = rect[1][1]
            aspect_ratio = width / height# 计算长宽比

            if 0.6 < aspect_ratio < 1.4 and width * height > 2000:
                # 计算中心点
                moments = cv2.moments(box)
                center_x = int(moments['m10'] / moments['m00'])
                center_y = int(moments['m01'] / moments['m00'])
                center_list.append((center_x,center_y))
                cv2.drawContours(result, [box], 0, (0, 255, 0), 2)# 绘制最小矩形
                cv2.circle(result, (center_x, center_y), 5, (0, 0, 255), -1)# 绘制中心点

    # cv2.imshow('dilated',dilated)
    # cv2.imshow('result',result)
    return (
        center_list,
        dilated,
        result
    )